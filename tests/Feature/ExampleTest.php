<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {

        $response = $this->json('POST', '/check', ['domain' => 'php.net','amnserver.com']);
        $response->assertStatus(200);

        $firstpage = $this->get('/');
        $firstpage->assertSuccessful();

    }
}
