<?php

namespace App\Http\Controllers;

use App\Services\Network;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    private $net;

    public function __construct(Request $request)
    {
        $this->net = new  Network($request->domain, 80, 5);
    }


    public function checkAll()
    {
        try {

            if (false) {
                return response()->json(
                    ["data" =>
                        ["message" => "تصویر امنیتی اشتباه است."]]
                    ,401 );
            } else {
                $data = [];
                //    Check SSL
                $data['ssl'] = $this->net->checkSsl();

                //    Check NS Record
                $data['ns'] = $this->net->checkNSRecord(['ns10.parspack.com', 'ns11.parspack.com', 'ns12.parspack.com', 'ns13.parspack.com'],true);

                //    Check A record
                $data['record'] = $this->net->checkDnsRecord('A',true);

                //    Check Ping
                $data['ping'] = $this->net->checkPing();

                //    Check IPAddress
                $data['ipAddress'] = $this->net->checkIpAddress('130.185.74.60',true);

                return response()->json(
                    ["data" => $data,"domain" => request()->domain],200);
            }


        } catch (Exception $e) {
            return $e->getTrace();
        }

    }


}
