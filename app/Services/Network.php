<?php

namespace App\Services;

class Network
{

    // Variable
    private  $dns;

    // Variable
    private  $host;

    // Variable
    private $port;

    // Variable
    private  $timeout ;


    public function __construct($host,$port,$timeout)
    {
        $this->dns = dns_get_record($host);
        $this->host = $host;
        $this->port = $port;
        $this->timeout = $timeout;
    }


     /* Check DNS Record
     * return 200 If ok
     */
    public function checkNSRecord($searchableNs,$api = false)
    {
        $data = [];
        foreach ($this->dns as $struct) {
            if ($struct['type'] == 'NS') {
                $data[] =  $struct['target'];
            }
        }
        if ($api){
            $ns = (implode(" و ",$data)) == "" ? "  رکورد NS  یافت نشد . " : nl2br(" رکورد های دامنه شما با " .implode(" و ",$searchableNs) . " برابر نیست و به درستی ست نشده است . ");
            return  empty(array_diff($searchableNs,$data)) ?  ["service" => 'NS',"code" => 200 , "message" =>  $ns]  : ["service" => 'NS',"code" => 200 , "message" => nl2br($ns)];
        }else{
            $ns = (implode(" و ",$data)) == "" ? " - می باشد " : nl2br(" رکورد های دامنه شما : " . "\n"  .implode(" و ",$data) . " می باشد . ");
            return  empty(array_diff($searchableNs,$data)) ?  ["service" => 'NS',"code" => 200 , "message" =>  $ns]  : ["service" => 'NS',"code" => 200 , "message" => nl2br($ns)];
        }
    }


    /* Check Dns Record
     * return 200 If ok
     */
    public function checkDnsRecord($recordType,$api = false)
    {
        foreach ($this->dns as $struct) {
            if($api == false){
                if($struct['type'] == $recordType){
                    return  ["service" => $recordType ,"code" => 200 , "message" => " ای پی :  " . $struct['ip']];
                }
            }else{
                if($struct['type'] == $recordType){
                    return  ["service" => $recordType ,"code" => 200 , "message" => "  رکورد به درستی تنظیم شده است  " . $recordType ];
                }
            }
        }
        return  ["service" => $recordType,"code" => 500 , "message" => $recordType . " رکورد  پیدا نشد. "];
    }


    /* Check SSL is Valid
     * return 200 If ok
     */
    public function checkSsl()
    {
        try {
            $orignal_parse = parse_url("http://" . $this->host, PHP_URL_HOST);
            $get = stream_context_create(array("ssl" => array("capture_peer_cert" => TRUE)));
            $read = stream_socket_client("ssl://".$orignal_parse.":443", $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $get);
            $cert = stream_context_get_params($read);
            $certinfo = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);
                return ["service" => 'SSL',"code" => 200 , "message" => " SSL دارد :   (تاریخ پایان  : " . date('H:i:s Y/m/d', $certinfo['validTo_time_t']) .")"];
        }catch (\Exception $e){
            return ["service" => 'SSL',"code" => 500 , "message" => "برای دامنه SSL  نظر تنظیم نشده است . "];
        }
    }


    /* Check IpAddress is Valid
     * return 200 If ok
     */
    public function checkIpAddress($ipAddress,$api = false)
    {
        foreach ($this->dns as $struct) {
            if($api == false){
                if ($struct['type'] == "A") {
                    return  $struct['ip'] == $ipAddress ? ["service" => 'ای پی',"code" => 200 , "message" => "دامنه مورد نظر به درستی تنظیم شده است . "] : ["service" => 'ای پی',"code" => 500 , "message" => " ای پی دامنه مورد نظر با ای پی " . $ipAddress  . " همسان نیست . "];
                }
            }else{
                return  $struct['ip'] == $ipAddress ? ["service" => 'ای پی',"code" => 200 , "message" => "دامنه مورد نظر به درستی تنظیم شده است . "] : ["service" => 'ای پی',"code" => 500 , "message" => " ای پی دامنه مورد نظر با ای پی " . $ipAddress  . " همسان نیست . "];
            }
        }
    }



    /* CHeck Ping Of Domain
     * return 200 If ok
     */
    public  function checkPing()
    {
        $fP = fSockOpen($this->host, $this->port, $errno, $errstr,  $this->timeout);
        if (!$fP) { return 500; }
        return ["service" => 'پینگ',"code" => 200 , "message" => "  پینگ دارد . "];
    }
}