<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" style="direction: rtl">
<head>
    <title>ParsPack</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        @if (Route::has('login'))
            <div class="top-right links">
                @if (Auth::check())
                    <a href="{{ url('/home') }}">Home</a>
                @else
                    <a href="{{ url('/login') }}">Login</a>
                    <a href="{{ url('/register') }}">Register</a>
                @endif
            </div>
        @endif
        <div class="wrap-login100">
            <div class="login100-form validate-form">
                <div class="logo-div"><img class="logo" src="images/logo.png" alt=""></div>
                <span class="login100-form-title" id="message"></span>
                <div id="form">
                    <div class="wrap-input100 validate-input" data-validate="لطفا دامنه مورد نظر را وارد نمایید ...">
                        <input value="{{old('domain')}}" class="input100" type="text" id="domain" name="domain"
                               placeholder="مانند : parspack.com">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
							<i class="fa fa-wikipedia-w" aria-hidden="true"></i>
						</span>
                    </div>

                    <div class="wrap-input100 validate-input" style="height: 58px;">
                        <div style="float: right;width: 70%;">
                            <input class="input100" type="text" id="captcha" name="captcha" placeholder="تصویر امنیتی">
                        </div>
                        <div class="captcha" style="float: right;width: 30%;">
                            <img id="captchaimage" src="{{captcha_src('flat')}}" alt="captcha">
							<i id="refresh" class="fa fa-refresh" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>


                <div>
                    <ul id="result"></ul>
                </div>
                <div class="container-login100-form-btn">
                    <button class="login100-form-btn" id="check" type="button">
                        بررسی
                    </button>
                </div>


            </div>
        </div>
    </div>
</div>


<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/tilt/tilt.jquery.min.js"></script>
<script>
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>
<!--===============================================================================================-->
<script src="js/main.js"></script>
<script>
    var input = document.getElementById("domain");
    input.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("check").click();
        }
    });
    var input = document.getElementById("captcha");
    input.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("check").click();
        }
    });

    $("#check").click(function () {
        $("#result").html("")

        if ($("#domain").val() != "") {
            $.ajax(
                {
                    url: '{{route('checkDomain')}}',
                    type: 'post',
                    data: {'domain': $("#domain").val(), 'captcha': $("#captcha").val()},
                    beforeSend: function () {
                        $("#result").append("لطفا منتظر بمانید...")
                    },
                    success: function (data) {
                        var x
                        var data = data.data
                        $("#result").html("")
                        for (x in data) {
                            if (data[x].code == 200) {
                                $("#result").append("<li style='color: green'>" + ' ' + data[x].message + "</li><br>")
                            }
                            // else {
                            //     $("#result").append("<li style='color: red'>" + ' ' + data[x].message + "</li><br>")
                            // }
                        }
                        $("#refresh").click()
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                       if(xhr.status == 401){
                           $("#result").html("").append("تصویر امنتی اشتباه است . ");
                           $("#refresh").click()
                       }
                    }
                });
        } else {
            $("#domain").css("border", "1px solid red")
        }

    })

    $("#refresh").click(function () {
        $.ajax(
            {
                url: '{{route('getcaptcha')}}',
                type: 'get',
                success: function (data) {
                    $("#captchaimage").attr('src',data)
                },
                error: function (xhr, ajaxOptions, thrownError) {
                }
            });
    })
</script>
<style>
    .captcha img {
        width: 80%;
        margin-top: 5px;
        border-radius: 25px;
    }


    i#refresh {
        cursor: pointer;
    }
    .logo{
        background: #7d86ca;
        padding: 10px;
        border-radius: 9px;
    }
    .logo-div {
        margin: auto;
        width: 100%;
        text-align: center;
    }
    ul#result {
        margin-right: 30px;
    }
</style>
</body>
</html>